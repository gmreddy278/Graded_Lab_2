$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/resources/emp.feature");
formatter.feature({
  "line": 1,
  "name": "Eligible Employee names",
  "description": "",
  "id": "eligible-employee-names",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1633268,
  "status": "passed"
});
formatter.scenario({
  "line": 2,
  "name": "Eligible Employee names checking",
  "description": "",
  "id": "eligible-employee-names;eligible-employee-names-checking",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "the below Data:",
  "rows": [
    {
      "cells": [
        "Nolan"
      ],
      "line": 4
    },
    {
      "cells": [
        "sorrentino"
      ],
      "line": 5
    },
    {
      "cells": [
        "polanski"
      ],
      "line": 6
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "User Navigates  Shipment Page",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "checks valid employee names",
  "keyword": "Then "
});
formatter.match({
  "location": "PriceStepsDefinition.the_below_Data(DataTable)"
});
formatter.result({
  "duration": 240226498,
  "status": "passed"
});
formatter.match({
  "location": "PriceStepsDefinition.user_Navigates_Shipment_Page()"
});
formatter.result({
  "duration": 4449420012,
  "status": "passed"
});
formatter.match({
  "location": "PriceStepsDefinition.checks_valid_employee_names()"
});
formatter.result({
  "duration": 24990,
  "status": "passed"
});
formatter.uri("src/resources/items.feature");
formatter.feature({
  "line": 1,
  "name": "Price is correct or not",
  "description": "Ensure price is correct on shipment.jsp",
  "id": "price-is-correct-or-not",
  "keyword": "Feature"
});
formatter.before({
  "duration": 35253,
  "status": "passed"
});
formatter.before({
  "duration": 4321292816,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "upc and price",
  "description": "",
  "id": "price-is-correct-or-not;upc-and-price",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "the following Data:",
  "rows": [
    {
      "cells": [
        "567321101987",
        "19.99"
      ],
      "line": 7
    },
    {
      "cells": [
        "567321101986",
        "17.99"
      ],
      "line": 8
    },
    {
      "cells": [
        "567321101985",
        "20.49"
      ],
      "line": 9
    },
    {
      "cells": [
        "567321101984",
        "23.88"
      ],
      "line": 10
    },
    {
      "cells": [
        "467321101899",
        "9.75"
      ],
      "line": 11
    },
    {
      "cells": [
        "477321101878",
        "17.25"
      ],
      "line": 12
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "User Navigates to Shipment Page",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "price from the page should match the given price for each upc code",
  "keyword": "Then "
});
formatter.match({
  "location": "PriceStepsDefinition.the_following_Data(DataTable)"
});
formatter.result({
  "duration": 901867,
  "status": "passed"
});
formatter.match({
  "location": "PriceStepsDefinition.user_Navigates_to_Shipment_Page()"
});
formatter.result({
  "duration": 1230434030,
  "status": "passed"
});
formatter.match({
  "location": "PriceStepsDefinition.price_from_the_page_should_match_the_given_price_for_each_upc_code()"
});
formatter.result({
  "duration": 578217918,
  "status": "passed"
});
formatter.after({
  "duration": 29452,
  "status": "passed"
});
formatter.before({
  "duration": 227586,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Checking whether employee Id is correct or not",
  "description": "",
  "id": "price-is-correct-or-not;checking-whether-employee-id-is-correct-or-not",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "The employee first name is \"Nolan\"",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "user navigates to confirm shipment page",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "the displayed employee id is $111",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Nolan",
      "offset": 28
    }
  ],
  "location": "PriceStepsDefinition.the_employee_first_name_is(String)"
});
formatter.result({
  "duration": 1290103,
  "status": "passed"
});
formatter.match({
  "location": "PriceStepsDefinition.user_navigates_to_confirm_shipment_page()"
});
formatter.result({
  "duration": 5103419332,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "111",
      "offset": 30
    }
  ],
  "location": "PriceStepsDefinition.the_displayed_employee_id_is_$(int)"
});
formatter.result({
  "duration": 450667275,
  "status": "passed"
});
});