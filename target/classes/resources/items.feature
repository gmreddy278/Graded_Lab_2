Feature: Price is correct or not 
	Ensure price is correct on shipment.jsp

@Web 
Scenario: upc and price 
	Given the following Data: 
		|567321101987	| 19.99|
		|567321101986	| 17.99|
		|567321101985	| 20.49|
		|567321101984	| 23.88|
		|467321101899	|  9.75|
		|477321101878	| 17.25|
		
		
	When User Navigates to Shipment Page 
	
	Then price from the page should match the given price for each upc code 
	
Scenario: Checking whether employee Id is correct or not 
	Given The employee first name is "Nolan" 
	When user navigates to confirm shipment page
	Then the displayed employee id is $111