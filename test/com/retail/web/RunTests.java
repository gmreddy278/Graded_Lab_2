package com.retail.web;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/Destination"},
		features = {"src/resources/items.feature", "src/resources/emp.feature" } )
		//features = {"src/resources/items.feature", "src/resources/emp.feature" } )
		
public class RunTests {

	
}
