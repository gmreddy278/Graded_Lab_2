package com.retail.web;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import java.io.File;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.OutputType;

public class SeleniumChromeAutoLoginTestScreenshot {

	WebDriver driver = new ChromeDriver();

	@Test
	public void test() {

		String os = System.getProperty("os.name").toLowerCase().split(" ")[0];

		System.setProperty("webdriver.chrome.driver", "C:\\Softwares" + "\\chromedriver.exe");
		// driver = new ChromeDriver();

		WebDriver driver = new ChromeDriver();

		try {
			driver.get("http://localhost:8081/login?from=%2F");
			driver.manage().window().maximize();

			System.out.println("Test");

			WebElement username = driver.findElement(By.id("j_username"));

			WebElement password = driver.findElement(By.name("j_password"));

			WebElement submit = driver.findElement(By.name("Submit"));
			/*
			 * System.out.println(username); System.out.println(password);
			 * System.out.println(submit);
			 */
			Actions loginAction = new Actions(driver);
			loginAction.click(username).sendKeys("gmreddy278").sendKeys(Keys.TAB).sendKeys("gundra.123")
					.sendKeys(submit).click();
			loginAction.perform();

			this.takeSnapShot(driver);

			// System.out.println("");

		} catch (Exception e) {
			System.err.println(e.getMessage());

		}

	}

	public static void takeSnapShot(WebDriver webdriver) throws Exception {
		TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(SrcFile, new File("C://Users//mgundra//Desktop//" + "Jenkins_GMR.png"));

	}

	@After
	public void cleanup() {
		driver.close();

	}

}
