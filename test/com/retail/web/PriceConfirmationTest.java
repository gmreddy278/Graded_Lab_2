package com.retail.web;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PriceConfirmationTest {

	private List<Double> priceList;
	private List<String> upcList;
	private ChromeDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Softwares" + "\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testDataTable() {

		driver.get("http://localhost:8080/SimpleRetailWeb_GMR/");
		String xpathVal = "//input[@value='Get Items To Be Shipped']";
		WebElement btn = driver.findElement(By.xpath(xpathVal));
		btn.submit();
		assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));

		List<WebElement> priceElements = driver.findElements(By.xpath("//tbody/tr/td[4]"));
		Double[] price = {19.99, 17.99, 20.49, 23.88, 9.75 , 17.25};
		for (int i = 0; i < priceElements.size(); i++) {
			
			assertEquals(price[i], Double.parseDouble(priceElements.get(i).getText()),.001);
		}
	}
	
	@Test
	public void testEmployeeId() {

		
		

		List<WebElement> priceElements = driver.findElements(By.xpath("//tbody/tr/td[4]"));
		Double[] price = {19.99, 17.99, 20.49, 23.88, 9.75 , 17.25};
		for (int i = 0; i < priceElements.size(); i++) {
			
			assertEquals(price[i], Double.parseDouble(priceElements.get(i).getText()),.001);
		}
	}

}
