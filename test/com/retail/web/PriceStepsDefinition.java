package com.retail.web;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.gargoylesoftware.htmlunit.javascript.host.Iterator;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.eo.Do;

public class PriceStepsDefinition {
	private ChromeDriver driver;

	private List<String> empNames;
	private Map<String, Double> dataTable;
	private Map<String, Double> webElements;
	private String empName;

	@Before("@Web")
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "C:\\Softwares" + "\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("^the following Data:$")
	public void the_following_Data(DataTable itemValues) throws Throwable {

		dataTable = itemValues.asMap(String.class, Double.class);

		/*
		 * for (Map.Entry<String, Double> entry : dataTable.entrySet()) {
		 * System.out.printf("Key : %s and Value: %s %n", entry.getKey(),
		 * entry.getValue()); }
		 */

	}

	@When("^User Navigates to Shipment Page$")
	public void user_Navigates_to_Shipment_Page() throws Throwable {
		driver.get("http://localhost:8080/SimpleRetailWeb_GMR/");
		String xpathVal = "//input[@value='Get Items To Be Shipped']";
		WebElement btn = driver.findElement(By.xpath(xpathVal));
		btn.submit();
		assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));

	}

	@Then("^price from the page should match the given price for each upc code$")
	public void price_from_the_page_should_match_the_given_price_for_each_upc_code() throws Throwable {
		List<WebElement> priceElements = driver.findElements(By.xpath("//tbody/tr/td[4]"));
		List<WebElement> upcElements = driver.findElements(By.xpath("//tbody/tr/td[2]"));
		webElements = new HashMap<>();
		for (int i = 0; i < priceElements.size(); i++) {
			webElements.put(upcElements.get(i).getText(), Double.parseDouble(priceElements.get(i).getText()));
			// System.out.println(" test : "+ i +" : "+upcElements.get(i).getText());
		}
		// Checks whether two maps are equal or not
		System.out.println("dataTable.equals(webElements) : " + dataTable.equals(webElements));

		for (int i = 0; i < priceElements.size(); i++) {
			// System.out.println(" dataTable.get(priceElements.get(i))
			// :"+dataTable.get(upcElements.get(i).getText()));
			// dataTable.get(upcElements.get(i).getText()).
			// System.out.println(" priceElements.get(i) :"+priceElements.get(i).getText());
			// assertEquals(dataTable.get(priceElements.get(i)), priceElements.get(i),.001);
			assertEquals(dataTable.get(upcElements.get(i).getText()),
					Double.parseDouble(priceElements.get(i).getText()), .001);
		}
	}

	// BDD for second scenario where Employee Id is validated

	@Given("^The employee first name is \"([^\"]*)\"$")
	public void the_employee_first_name_is(String name) throws Throwable {
		empName = name;

	}

	@When("^user navigates to confirm shipment page$")
	public void user_navigates_to_confirm_shipment_page() throws Throwable {
		System.out.println("Here");
		driver = new ChromeDriver();
		driver.get("http://localhost:8080/SimpleRetailWeb_GMR/");
		String xpathVal = "//input[@value='Get Items To Be Shipped']";
		WebElement btn = driver.findElement(By.xpath(xpathVal));
		btn.submit();
		System.out.println("THere");
		assertTrue(driver.getCurrentUrl().contains("ShippingServlet"));

	}

	@Then("^the displayed employee id is \\$(\\d+)$")
	public void the_displayed_employee_id_is_$(int id) throws Throwable {
		List<WebElement> checkElements = driver.findElements(By.xpath("//tbody/tr/td[1]"));
		checkElements.get(0).click();
		checkElements.get(1).click();
		String xpathVal = "//input[@value='Create Shipment']";
		WebElement btn = driver.findElement(By.xpath(xpathVal));
		btn.submit();
		String empid = driver.findElement(By.id("employeeId")).getText();
		assertEquals(id, Integer.parseInt(empid));
	}

	@Given("^the below Data:$")
	public void the_below_Data(DataTable names) throws Throwable {
		empNames = names.asList(String.class);

	}

	@When("^User Navigates  Shipment Page$")
	public void user_Navigates_Shipment_Page() throws Throwable {
		driver = new ChromeDriver();
		List<WebElement> emps = driver.findElements(By.id("empId"));
		for (int i = 0; i < emps.size(); i++) {
			assertEquals(empNames.get(i), emps.get(i));
		}

	}

	@Then("^checks valid employee names$")
	public void checks_valid_employee_names() throws Throwable {

	}

	@After("@Web")
	public void tearDown() {
		// driver.close();
		// driver.quit();
	}

}
