package com.retail.web;

import static org.junit.Assert.*;

import org.junit.Test;

import com.retail.core.Employee;
import com.retail.core.Employees;

import cucumber.api.java.Before;

public class EmployeeTestCase {

	Employees employees;
	Employee employee;
	
	@Before
	public void setUp() {
		employees = new Employees();
		employee = new Employee();
	}
	
	@Test
	public void testTotalEligibleEmployeeSize() {
		assertEquals(3, employees.getEligibleEmployees().size());
	}
	
	@Test
	public void testOverWorkEmployee() {
		String expected = "woody";
		String actual= null;
		for(Employee e: Employees.getTotalEmployees()) {
			if(e.getHoursWorked()>40)
				actual = e.getFirstName();
		}
		assertEquals(expected, actual);
		
	}

}
