package com.retail.core;

public class Employee {

	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastname;
	private int employeeId;
	private double hoursWorked;
	
	public Employee() {
		
	}
	
	public Employee(String firstName, String lastname, int employeeId, double hoursWorked) {
		super();
		this.firstName = firstName;
		this.lastname = lastname;
		this.employeeId = employeeId;
		this.hoursWorked = hoursWorked;
	}
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public double getHoursWorked() {
		return hoursWorked;
	}
	public void setHoursWorked(double hoursWorked) {
		this.hoursWorked = hoursWorked;
	}

	@Override
	public String toString() {
		return "Employee [firstName=" + firstName + ", lastname=" + lastname + ", employeeId=" + employeeId
				+ ", hoursWorked=" + hoursWorked + "]";
	}
	
	
}
