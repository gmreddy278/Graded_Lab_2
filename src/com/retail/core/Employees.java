package com.retail.core;

import java.util.ArrayList;
import java.util.List;

public class Employees {

	
	private static List<Employee> employees;
	public Employees() {
		employees = new ArrayList<>();
		employees.add(new Employee("Chris", "Nolan", 111, 38));
		employees.add(new Employee("paulo", "sorrentino", 112, 11));
		employees.add(new Employee("woody", "allen", 113, 41));
		employees.add(new Employee("roman", "polanski", 114, 21));
	}
	
	
	
	public static List<Employee> getTotalEmployees() {	
		new Employees();
		return employees;
	}
	
	public static List<Employee> getEligibleEmployees() {
		new Employees();
		List<Employee> eligibleEmployees = new ArrayList<>();
		for(Employee e : employees) {
			if(e.getHoursWorked()<40)
				eligibleEmployees.add(e);
		}
		//System.out.println("eligibleEmployees size :: "+ eligibleEmployees.size());
		return eligibleEmployees;
		
	}
}
