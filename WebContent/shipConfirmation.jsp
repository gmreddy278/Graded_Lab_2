<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shipment JSP</title>
</head>
<body>

	<table border='1'>
		<c:forEach var="item" items="${sessionScope.shipItems}">
			<tr>
				<td>${item}</td>
			</tr>
		</c:forEach>
	</table>
<br><br>

<p>your items will be delivered by Employee with Id : <h1 id="employeeId">${employeeId}</h1> 
</p>

<form name='home' action="index.html" >
  <input type='submit' value="Home">
</form>

</body>
</html>